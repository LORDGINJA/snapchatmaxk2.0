package com.example.student.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class LoggedInActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LoggedInFragment loggedin = new LoggedInFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, loggedin).commit();
    }
}
